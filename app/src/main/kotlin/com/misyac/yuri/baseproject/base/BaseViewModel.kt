package com.misyac.yuri.baseproject.base

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {

    private val disposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        disposable.takeIf { !it.isDisposed }?.dispose()
    }

    protected fun addDisposable(disposable: Disposable) {
        this.disposable.add(disposable)
    }

}