package com.misyac.yuri.baseproject.base

import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.module.AppGlideModule
import com.misyac.yuri.baseproject.App
import okhttp3.OkHttpClient
import java.io.InputStream
import javax.inject.Inject

@GlideModule
class GlideConfiguration : AppGlideModule() {

    @Inject
    lateinit var client: OkHttpClient

    init {
        App.appComponent.inject(this)
    }

    override fun registerComponents(context: Context?, glide: Glide?, registry: Registry?) {
        registry?.append(GlideUrl::class.java, InputStream::class.java, OkHttpUrlLoader.Factory(client))
    }

}
