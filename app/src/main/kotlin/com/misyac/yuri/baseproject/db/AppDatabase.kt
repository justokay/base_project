package com.misyac.yuri.baseproject.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = arrayOf(User::class), version = AppDatabase.VERSION)
abstract class AppDatabase: RoomDatabase() {

    companion object {
        const val VERSION: Int = 1
        const val NAME: String = "app.db"
    }

    abstract fun userDao(): UserDao

}