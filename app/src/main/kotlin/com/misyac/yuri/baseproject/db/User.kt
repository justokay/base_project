package com.misyac.yuri.baseproject.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity()
class User {

    @PrimaryKey(autoGenerate = true)
    var id: Long? = null
    var name: String = ""
    var lastName: String? = null

    override fun toString(): String {
        return "User(id=$id, name='$name', lastName=$lastName)"
    }


}