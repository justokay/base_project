package com.misyac.yuri.baseproject.di

import com.misyac.yuri.baseproject.di.scope.ActivityScope
import com.misyac.yuri.baseproject.ui.main.MainActivity
import com.misyac.yuri.baseproject.ui.main.di.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun mainActivity(): MainActivity

}