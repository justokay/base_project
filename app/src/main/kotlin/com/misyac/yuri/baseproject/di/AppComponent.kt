package com.misyac.yuri.baseproject.di

import com.misyac.yuri.baseproject.App
import com.misyac.yuri.baseproject.base.GlideConfiguration
import com.misyac.yuri.baseproject.di.module.ApiModule
import com.misyac.yuri.baseproject.di.module.AppModule
import com.misyac.yuri.baseproject.di.module.DBModule
import com.misyac.yuri.baseproject.di.module.DaggerViewModelInjectionModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class,
    ApiModule::class,
    DBModule::class,
    ActivityBindingModule::class,
    DaggerViewModelInjectionModule::class
])
interface AppComponent : AndroidInjector<DaggerApplication> {

    fun inject(app: App)

    override fun inject(instance: DaggerApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(appComponent: App): AppComponent.Builder

        fun build(): AppComponent
    }

    fun inject(app: GlideConfiguration)

}
