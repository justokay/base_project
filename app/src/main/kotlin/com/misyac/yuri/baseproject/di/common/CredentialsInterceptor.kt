package com.misyac.yuri.baseproject.di.common

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CredentialsInterceptor @Inject constructor() : Interceptor {

    companion object {
        private val AUTHORIZATION = "Authorization"
    }

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()

        //        String token = AuthUtils.getToken();
        val response: Response
        //        if (TextUtils.isEmpty(token)) {
        response = chain.proceed(original)
        //        } else {
        //            String authorization = buildAuthorization(token);
        //            Request.Builder requestBuilder = original.newBuilder()
        //                    .header(AUTHORIZATION, authorization)
        //                    .header("Accept", "application/json")
        //                    .method(original.method(), original.body());
        //
        //            Request request = requestBuilder.build();
        //            response = chain.proceed(request);
        //        }

        if (response.code() == 401) {
            logout()
        }

        return response
    }

    private fun logout() {

    }

}
