package com.misyac.yuri.baseproject.di.module

import android.content.Context
import com.misyac.yuri.baseproject.App
import dagger.Binds
import dagger.Module

@Module/*(includes = [DaggerViewModelInjectionModule::class])*/
abstract class AppModule {

    @Binds
    abstract fun bindContext(app: App): Context

}
