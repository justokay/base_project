package com.misyac.yuri.baseproject.di.module

import android.arch.persistence.room.Room
import android.content.Context
import com.misyac.yuri.baseproject.db.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DBModule {

    @Singleton
    @Provides
    fun provideDb(context: Context): AppDatabase =
            Room.databaseBuilder(context, AppDatabase::class.java, AppDatabase.NAME).build()

}