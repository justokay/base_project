package com.misyac.yuri.baseproject.di.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.misyac.yuri.baseproject.di.ViewModelKey
import com.misyac.yuri.baseproject.di.common.DaggerViewModelFactory
import com.misyac.yuri.baseproject.ui.main.model.MainViewModel
import com.misyac.yuri.baseproject.ui.main.model.SecondViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class DaggerViewModelInjectionModule {

    @Binds
    abstract fun bindViewModelFactory(factory: DaggerViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SecondViewModel::class)
    abstract fun bindSecondViewModel(searchViewModel: SecondViewModel): ViewModel

}
