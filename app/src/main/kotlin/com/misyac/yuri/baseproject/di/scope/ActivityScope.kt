package com.misyac.yuri.baseproject.di.scope

import java.lang.annotation.Documented
import javax.inject.Scope

@Documented
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope