package com.misyac.yuri.baseproject.ui.main

import android.os.Bundle
import com.misyac.yuri.baseproject.R
import com.misyac.yuri.baseproject.base.BaseActivity
import com.misyac.yuri.baseproject.di.scope.ActivityScope

@ActivityScope
class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var fragment = supportFragmentManager.findFragmentByTag(MainFragment.tag)
        if (fragment == null) {
            fragment = MainFragment()
        }
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment, MainFragment.tag)
                .commit()

    }

}
