package com.misyac.yuri.baseproject.ui.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.misyac.yuri.baseproject.R
import com.misyac.yuri.baseproject.base.BaseFragment
import com.misyac.yuri.baseproject.base.GlideApp
import com.misyac.yuri.baseproject.databinding.FragmentMainBinding
import com.misyac.yuri.baseproject.db.User
import com.misyac.yuri.baseproject.ui.main.model.MainViewModel
import com.misyac.yuri.baseproject.ui.main.model.SecondViewModel
import timber.log.Timber
import javax.inject.Inject

class MainFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: MainViewModel
    private lateinit var secondViewModel: SecondViewModel
    private lateinit var binding: FragmentMainBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        secondViewModel = ViewModelProviders.of(this, viewModelFactory).get(SecondViewModel::class.java)
        subscribeViewAll()
        subscribeInsert()
        subscribeInsertStatus()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.add.setOnClickListener {
            binding.name.text?.takeIf { it.trim().isNotEmpty() }?.let {

                viewModel.insertUser(
                        User().apply {
                            name = it.toString()
                            lastName = binding.lastName.text.trim().toString()
                        }
                )

            }

        }

        binding.result.setOnClickListener {
//            viewModel.getResult()
            secondViewModel.message()
        }

        GlideApp.with(this)
                .load("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAh1BMVEX///8AAAD8/Pzy8vKjo6O4uLienp6+vr7v7+/5+fnIyMjW1tbn5+fNzc329vbj4+M+Pj7c3NyHh4eOjo6pqaktLS2wsLCAgIB1dXUYGBhGRkZkZGSamppSUlLS0tJwcHAkJCQ3NzcSEhIeHh5dXV1paWlFRUU7Ozt7e3sTExMiIiJNTU1XV1cA2oSxAAAOD0lEQVR4nO1diXLiOBA14rAxxmAOQwIBTEImCfn/71vLFz6k1m0xW/OqdmprBlt6ltTd6m61HOcf/kEJi0mMbPfBLAaDwdp2H4xinDIc2O6EUWQMQ9u9MIkTZhjY7oVJRJjhwnYvTAJhhnPbvTAIhGXpYGy7G0ZxTRnGtjtRAiGsm4PZLAo8Xa90XlKGR01v04H5eYk/+uBledRCEjmXZ1L5aJ2xK7DR89Jz+qo3p2/DDRX/NeHX+Q3ep3ra+soZ9kvx0Vq94eOgAV3SYZS+K+mboRO54/Vq7c7qZNdNgh+6JhZmeO13lgbrbUnjZVJwRE7cJHjR1iW3V4bpXBnemlT8/F+C5t/eh9ra9PH79L2OAZS310Cuq74af3fW9smRM8Mv7Mn0Tkdw0iE4GNxK26rEdkyStIJtFbIFobDB0KzEQW1xWeA11e27B79Yg5qoiISBm02Uue9HYdELYyRRtuiJwLb/bLNO5avvqY9fjmB+vCXNZn7e9qOZyUEMaQQHg31tGilMJFQYE4vRjd7Wdhd7TmkE68UHvdVUeq7cRTmNlBD65yXUUIaPWNtcqaErRjtf9/WwmqutwtNuy2wmw/Xma+L1wBtf0997KeWF51w44aSX42USZo/pGssTf9NHqVaDL/ab21h7GhXIWaDhN/G9YSTy/hZHPRgKtZsE/KOIFZwnMX4lJpo0JFUXknHhXox4ko3k+aVI5lrW4o7dUgMCu6e5kHwh4aDDMk9EW+V1YrQ3K3KAWyP7JFq/EW7zyhdscK/sV/HgLaAL1cKIhzsyE2+TZxDDvR5+GC61lZTb5pdlIIzFW/yEPxuWgIuLPoKDwRe1He8OfoEMG/EGrwE4LxD1s32+ShJfEnVj6WFZwQxJW18I99WcqYpXxCczT/IMtPKpuJKCVMNiJUQ6GR5m2ccDxzAky9AyCHOXopjuVButpv/v5rrowiAoxpAx47OWQ/IW6VD+IiD+Mxtxa/EXTs4bc8sjwPCyYBrDCHnv5IfLj4OcgyTFmghPRVlS8nZYFAUYRiyC6b8GNC0YVT9ZU37BQbFqvpCPf1gzVIzhmKVaU/7BC+3pUmvJj2Eximkr5UpfcdmP3NrizrFfoxMcnMvfAE4hJuLsE83zhfDO6QuY876dIyDtQc/nOobsmeUGXs3FVuGX1yjnFm2sFyHG6Lzna0bChqpjFhWSbMO/xeE0kJfMFw1ZnrTbxN2o7jeKZbBki/UHOBf+nvEahOSsFSnsEFNH1EB26HfAYtiOwpmEK7b159w+HRivQcjfadoRMvCK3fACDNGU09XAeGm2LPwzXV1ow0HYd8NpY/A597zY/HKcCBJ0Fnzv5UruwSMZjCS3DxW2x3ngAl9KMN8PcXrbfjjFM/7V4vijQLAQanTN+Ss6T6d8O++RmADbyS7JZfml6M7WkyhFj08ICsTcs7iKe5YSrlW+Dt3c+hDjhx06XOLhUzhn2XN/xRkG1Ueir+eTaE9SOf/F8b3ZW/wugkkiRvC7FNrQPoulncmYztzJAVo8DJ8WAblwWogFDh7eAMDQFU2fRo/geXCkuCEGXwpREl/A4v7NcwZIKT4PiH/tkqmDA7Za31kg2LzyUjzm3xvcub7IMpyeMnVG6IzAfoz0YvzwbEWbHi3cIjyX4N9I58C/vWS+gbZsvWANNFWO5SHIUqlj+4f1C+kptUlNimnKpmGO/8R8cSweeBNm5/kg2xusZbepoAqT8k3Xj3FGLtCTOYsXmJwl0IL0NM2mUcrJy3T19RbnSRFogncXGgYxi817m09lhtJJ8Mfq8dPI9cK8U46fDDgcpiJEI3L4hh+vsk1H+fOHnFuuJU95TpH41pMObLfG7CQwCJLZKJUhca2c1P6r4jvJDeGvFe2+5RlK2KZZww/P1G8cBdF4lzzeaeAMzzDmzDnrQvLYB4I8U6KbFp7m8EDKMTxLLppm3nMLms6StBp0pmOZFfkpzRDwTJk7aiYxkO/SYgGYpjzxJyngDM2Y2zQvwBM8JANw2ww7M2N6xKm1qtmgeY7eSWwg5Q+gAtO0q/SHqVm+9zOrVcPwDmMBq3Uk3QwwTbt+vNJCcLVMX2w+cbt2dtKtTBPqS6+d1f1I3syS0Ku0fAWOC84ACDNYRAcwTTvOqEaU/G0TIR3T1dvwqI83+QaAaXpr/7b9NZbrk/LBIfyVfHZccyndEBSOuraj54SOvOzdajJLdSJ/KGDtPhJpAwRU+u1pSplOyXqO1NYkfjCGI38KCcTAAYW2NQh04LBZqFT2yHSkDzl2FBgiIHTU9EeBCSaDwXY/XlS9FR7O7AHAnlNJAgeWQNOSiGCGGMudj4dS/mSYN6F44lUYAtO0qWc502OWK19+X4IzLYm7yJkCQ2h51c+0ixyoSNaliJWZridCXEDe9E7fCBx1eUxTxBs/rvC6cxfT4lFRBKu2y0OJIRAVaUxTiZB98hVH4vzwJ2kvSBWGCDiGsXXKeZa2Khmvf/mYzIux5CeLxXHDSFRZh+D0q16M0FSOYI7t19hDgixT2fZwJiswhIN3q4eo4MxUoeP7PDl5AsOY5SWVglVJlqKQvtNPSqWP+LNTQSzPk8grCfDQPeXWsKL/FpCmUbkONabrXe87Pyp1JivdOp08+KyF4sE2YHjKoIiwsmCitPJYQNnZW0WGIV1MLivbVD4rHcDnejT3KhMIEU+S4r8aqm5EgdNnVe6QpmgnAdvbajwPtBQCoAKQpmUWH+34iD6ev+fN3FD9EwS598ukb2Vlwcnzvtu4UVjrGvlspaApCNqmxSrRoyy4cflYjdxTSCAitTEDlX4RXOwxt7uG7+1tv16P3QatoRdEnqBLAZqmhbtZ+giTDtzLbqZyf/OBPTrX5HZcCAkm+jQtRI3Gw77iyPc4eCAbiWoH4GR0B3S/aRFmk47f6sC4yPEJ2jErkfQt6u4vNzwA938PiPJNCUGer/hHkeajSPJ/9no4e0DFBfcAtbK3Chy5R3FIoVCcY5M98KoFRWIbWRTw5/pSaoAVk0CiWoE+5AqL4hS8cnv2EGkOvJT7MsWTdmrIukeV5vylX0nDNCsXsk11mCd9hTRJsOVm2D1Fi09KFqmYNtVhPkh0w5h3/486R6EPtX24eoKhPHI/A90w5q8yjRqr7dOvGUVIISlNFdd8odBlnUAdbeRMR8vva2rwfk4WdSseiVWY0osiyKeFYQYvmHXrXNtUFjGrC4IMUe3PBwRraGlFoJkhmbVaIS8llEkKdKtKSz17hVJsqihTaeiiQAtDi3unsv9GGeaF8S2hdBsbZtgjoxaSUuqZnaWMNAyTqIK0ZhkKVAXVjapui1mGFvdOlcvULEOlQjNKeCRjm2WoO7LGj0dysFmGoonn+vDIwTDL0FxkjYHrow9GGYb91GohoJb9bJQhR86eIdQ6b5Qhu865KdQ2qkYZ2omspbjX9qlGGVpzJdaPxxplaM2VWD9ZaZShrd3hz7AWWKIzlD8ylAPZi6x9OFwMhYtJdUAt42kajcOx5hgie+qwkRVmcgx7TjSpcGn0wuQY2nIlNo8KmBxD1ZIIsmiWZTXJUPJeDmU0k4IMMkSWImv3ZjcMMqQGXw2jFb02yNBWGkbriIVBhpb2Tt+tVCCDDCWuVNCBcyvbySBDS8qiXbbCIENLyqKdEG2QoWoRVjl0KicZZKh8z5EUOkXazDFUKY6vgE4lfXMMe0rSb+G7kw5ijqGduFP3Fm9zDDkLuWvGpDeGCL7X0xi65w3NMbQjSrs9oVfwUGKYGk5W3FCE8mWIWuVFlWGfxCoQ6jYaYmhr70Q4UGmMoRV1SLolBVHDtGqz1A5DYsFS6g5AcQytJAsRLz+i7lPVTrKDhUFM4Zt49I525uVK+rEILGwPb+QDz5SsHv7zFhRYSA+mnUgjujV/VMo35eib36PMQRuIZH3Il1Ss0HvK1x/qaTSvqxN1pAuhvt0YwL0F01Y69ouSHK3Qt0oEz9tF9doOax33QGP0nF7KOMfk5VdzvO9jdRlTodcoMPtiO8cJtZc5Hva4D+aqMW+gBDB/NVVVcIh/XXVwW1hMzr3s97V3nBfZN5uazxbWXyudnyL+w3zWgtodMOowz1D08qO/juHFUKH052GoctXU38FQy1E7Uwy1eFZ1Xh2ineHac4+qKvPO7oJNhvgH00X8pVAWTPFCNA2AGD52PYvxSvLqHA07dkXwMcQI53lFJyFcQsuSVIQhBloIJsF3w4a9Q4BhVgVRLN3oap2f2BgiuBAcAURfd88Qm6WCSXHqqbAaIMpQxP+xtr8IHXGGAuFHnbfZKUCUIXfoam+osKcwBBnCaQAnd5UfOHrbPAs/zQyzLGDP80gFSq1BmCFkvT2DcuhAmCEU9LBvhBIgKmnAyjaS1xqahTBDKC3uKTR8G8IMoeij/c0gARBD4pBAyZvnvnvPA+Yevw0oDeD/z/D1aZRgDcIMoQAyT6Swd2hluH3CIRRnCNxio3CBqkFAopEcvIXG0MSVvKqA9uxkh3xCf+BPz53nAlTmk5wlAnjbkp47zwNEz2OlZUECK1fh7k1jQNBZPbLcAEqfPqXGBzpMmXPAMeKn3Fs49MuhyHme0O0gktfemwZ1XZF/DjmF++04N2hnV0a0pFCqcNo9o1naLon9ADW0SR/E5/GvtUFaWGDslnAn5UDhrm3TQMjppri9gfMNkcTp/TnnqJMxnLbz90eguxM5QTfufQmelmGGRljwzLpCM6WYtAh+eE9NMB3G4boQqa8xewuEY6UNW+gyNpE8qR3Tme/nSWjszuIr/lZJ7uFfrmZcz/yNCKLZbPa8OuIfLOM/g7fBpGbtJGQAAAAASUVORK5CYII=")
                .into(binding.testImage)

    }

    private fun subscribeInsertStatus() {
        viewModel.insertStatus.observe(this, Observer {
            it?.let {
                Timber.e("Insert ${if (it) "started" else "finished"}")
            }
        })
    }

    private fun subscribeInsert() {
        viewModel.insert.observe(this, Observer {
            binding.name.text.clear()
            binding.lastName.text.clear()
            Toast.makeText(context, "${it?.name} has added", Toast.LENGTH_LONG).show()
        })
    }

    private fun subscribeViewAll() {
        viewModel.user.observe(this, Observer {
            Timber.e(it.toString())
        })
    }

    companion object {
        val tag: String = "mf"
    }

}