package com.misyac.yuri.baseproject.ui.main.di

import com.misyac.yuri.baseproject.di.scope.FragmentScope
import com.misyac.yuri.baseproject.ui.main.MainFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @FragmentScope
    @ContributesAndroidInjector/*(modules = [MainFragmentModule::class])*/
    abstract fun mainFragment(): MainFragment

}