package com.misyac.yuri.baseproject.ui.main.model

import android.arch.lifecycle.MutableLiveData
import com.misyac.yuri.baseproject.base.BaseViewModel
import com.misyac.yuri.baseproject.db.AppDatabase
import com.misyac.yuri.baseproject.db.User
import com.misyac.yuri.baseproject.db.UserDao
import com.misyac.yuri.baseproject.di.common.AppExecutors
import com.misyac.yuri.baseproject.utils.addDiskSchedulers
import io.reactivex.Flowable
import io.reactivex.rxkotlin.toSingle
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainViewModel @Inject constructor(
        db: AppDatabase,
        private val executor: AppExecutors
) : BaseViewModel() {

    private val userDao: UserDao = db.userDao()

    val insertStatus = MutableLiveData<Boolean>()
    val user = MutableLiveData<User>()
    val insert = MutableLiveData<User>()

    fun insertUser(user: User) {
        addDisposable(user.toSingle()
                .map {
                    userDao.insert(it)
                    it
                }
                .addDiskSchedulers(executor)
                .doOnSubscribe { insertStatus.value = true }
                .doAfterTerminate { insertStatus.value = false }
                .subscribe(
                        {
                            insert.value = it
                        },
                        {
                            Timber.e(it)
                        }
                )
        )
    }

    fun getResult() {
        addDisposable(userDao.getAllRx()
                .flatMapIterable { it }
                .concatMap {
                    Flowable.just(it).delay(1, TimeUnit.SECONDS)
                }
                .addDiskSchedulers(executor)
                .subscribe(
                        {
                            user.value = it
                        },
                        {
                            Timber.e(it)
                        }
                )
        )
    }


}