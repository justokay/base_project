package com.misyac.yuri.baseproject.ui.main.model

import android.content.Context
import android.widget.Toast
import com.misyac.yuri.baseproject.base.BaseViewModel
import java.lang.ref.WeakReference
import javax.inject.Inject


class SecondViewModel @Inject constructor(
        context: Context
): BaseViewModel() {

    private val weekContext: WeakReference<Context> = WeakReference(context)

    fun message() {
        weekContext.get()?.let {
            Toast.makeText(it, "Second View", Toast.LENGTH_LONG).show()
        }
    }

}