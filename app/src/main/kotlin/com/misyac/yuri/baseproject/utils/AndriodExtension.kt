package com.misyac.yuri.baseproject.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Environment
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.media.ExifInterface
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


const val TMP = "tmp"

fun Context.getResColor(@ColorRes id: Int) = ContextCompat.getColor(this, id)

fun Context.getResDrawable(@DrawableRes id: Int) = ContextCompat.getDrawable(this, id)

fun Context.showToast(message: String, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, length).show()
}

fun Context.clearTmpFolder() {
    val tmp = Environment.getExternalStorageDirectory().toString() +
            "/" +
            FileUtils.getApplicationFolderName(this) +
            "/" +
            TMP
    val folder = File(tmp)
    if (folder.exists() && folder.listFiles() != null) {
        for (file in folder.listFiles()) {
            file.delete()
        }
    }

    this.filesDir.listFiles()
            .filter { it.name == FileUtils.getApplicationFolderName(this) && it.listFiles() != null }
            .forEach {
                for (file in it.listFiles()) {
                    file.delete()
                }
            }
}

fun Context.createTmpFolder() {
    if (createExternalAppFolder(TMP)) {
        throw RuntimeException("Can't create tmp folder")
    }
}

fun Context.createTmpFile(name: String): File {
    val path = "${Environment.getExternalStorageDirectory()}/${getApplicationFolderName()}/$TMP"

    val fileName = FileUtils.getFileName(name)
    return File(path, fileName)
}

fun Context.createExternalAppFolder(path: String): Boolean {
    val builder = StringBuilder()
    builder.append(Environment.getExternalStorageDirectory())
            .append("/")
            .append(getApplicationFolderName())

    val appFolder = File(builder.toString())
    if (!appFolder.exists()) {
        if (!appFolder.mkdir()) {
            Timber.e("can't create " + builder.toString())
            return false
        }
    }

    for (srtFolder in path.split("[/]+".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
        builder.append("/")
                .append(srtFolder)

        val folder = File(builder.toString())
        if (!folder.exists()) {
            if (!folder.mkdir()) {
                Timber.e("can't create " + builder.toString())
                return false
            }
        }
    }

    return true
}

fun Context.getApplicationFolderName(): String {
    return applicationInfo.loadLabel(packageManager).toString()
}

@SuppressLint("MissingPermission")
fun Context.isOnline(): Boolean {
    val connectivityManager: ConnectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
    return (networkInfo != null && networkInfo.isConnected)
}

fun Context.getStatusBarHeight(): Int {
    var result = 0
    val resources = this.resources
    val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
    if (resourceId > 0) {
        result = resources.getDimensionPixelSize(resourceId)
    }
    return result
}

@Suppress("UNCHECKED_CAST")
fun <F : Fragment> AppCompatActivity.addFragment(containerId: Int, fragment: F, tag: String = "fragment", addBackStack: Boolean = false): F {
    val _fragment = supportFragmentManager.findFragmentByTag(tag) ?: fragment

    val beginTransaction = supportFragmentManager.beginTransaction()

    if (addBackStack) beginTransaction.addToBackStack(tag)

    beginTransaction
            .add(containerId, _fragment, tag)
            .commit()

    return _fragment as F
}

@Suppress("UNCHECKED_CAST")
fun <F : Fragment> AppCompatActivity.replaceFragment(containerId: Int, fragment: F, tag: String = "fragment", addBackStack: Boolean = false): F {
    val findFragment = supportFragmentManager.findFragmentByTag(tag) ?: fragment

    val beginTransaction = supportFragmentManager.beginTransaction()

    if (addBackStack) beginTransaction.addToBackStack(tag)

    beginTransaction
            .replace(containerId, findFragment, tag)
            .commit()

    return findFragment as F
}

fun Activity.hideKeyboard() {
    val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    //Find the currently focused view, so we can grab the correct window token from it.
    var view = currentFocus
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    if (view == null) {
        view = View(this)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Fragment.getResColor(@ColorRes id: Int) = context?.getResColor(id)

fun Fragment.getResDrawable(@DrawableRes id: Int): Drawable? = context?.getResDrawable(id)

fun Fragment.showToast(message: String, length: Int = Toast.LENGTH_SHORT) {
    context?.showToast(message, length)
}

fun RecyclerView.ViewHolder.getResColor(@ColorRes id: Int) = ContextCompat.getColor(itemView.context, id)

fun parseColorStateList(hex: String): ColorStateList = ColorStateList.valueOf(Color.parseColor(hex))

fun Drawable.changeColorTo(color: Int) {
    colorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP)
}

fun Drawable.setColor(color: Int) {
    colorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN)
}

fun Drawable.drawableToBitmap(): Bitmap {
    var bitmap: Bitmap? = null

    if (this is BitmapDrawable) {
        if (this.bitmap != null) {
            return this.bitmap
        }
    }

    bitmap = if (this.intrinsicWidth <= 0 || this.intrinsicHeight <= 0) {
        Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888) // Single color bitmap will be created of 1x1 pixel
    } else {
        Bitmap.createBitmap(this.intrinsicWidth, this.intrinsicHeight, Bitmap.Config.ARGB_8888)
    }

    val canvas = Canvas(bitmap!!)
    this.setBounds(0, 0, canvas.width, canvas.height)
    this.draw(canvas)
    return bitmap
}

fun Bitmap.getResizedBitmap(maxSize: Int): Bitmap {

    val bitmapRatio = width.toFloat() / height.toFloat()
    if (bitmapRatio > 1) {
        width = maxSize
        height = (width / bitmapRatio).toInt()
    } else {
        height = maxSize
        width = (height * bitmapRatio).toInt()
    }
    return Bitmap.createScaledBitmap(this, width, height, true)
}

/**
 * Rotated bitmap to normal state.
 *
 * @param source bitmap.
 * @param path   absolute image path.
 * @return
 */
fun Bitmap.rotateBitmap(path: String): Bitmap {
    val matrix = Matrix()

    var exif: ExifInterface? = null
    try {
        exif = ExifInterface(path)
    } catch (e: IOException) {
        Timber.e(e)
    }

    if (exif == null) {
        return this
    }
    val attributeInt = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)

    when (attributeInt) {
        ExifInterface.ORIENTATION_ROTATE_90 -> matrix.postRotate(90f)
        ExifInterface.ORIENTATION_ROTATE_180 -> matrix.postRotate(180f)
        ExifInterface.ORIENTATION_ROTATE_270 -> matrix.postRotate(270f)
        else -> matrix.postRotate(0f)
    }
    return try {
        Bitmap.createBitmap(this, 0, 0, this.width, this.height, matrix, true)
    } catch (e: Exception) {
        this
    }

}

fun Bitmap.blur(context: Context, radius: Float): Bitmap {
    val inputBitmap = Bitmap.createScaledBitmap(this, width, height, false)
    val outputBitmap = Bitmap.createBitmap(inputBitmap)

    val rs = RenderScript.create(context)
    val theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs))
    val tmpIn = Allocation.createFromBitmap(rs, inputBitmap)
    val tmpOut = Allocation.createFromBitmap(rs, outputBitmap)
    theIntrinsic.setRadius(radius)
    theIntrinsic.setInput(tmpIn)
    theIntrinsic.forEach(tmpOut)
    tmpOut.copyTo(outputBitmap)

    return outputBitmap
}

fun File.savePhoto(filePath: String): String {
    val out: FileOutputStream = FileOutputStream(this)

    try {
        out.use {
            BitmapFactory.decodeFile(File(filePath).absolutePath, BitmapFactory.Options())
                    .getResizedBitmap(1024)
                    .rotateBitmap(filePath)
                    .compress(Bitmap.CompressFormat.PNG, 100, out)
        }

        return absolutePath
    } catch (e: Exception) {
        Timber.e(e)
        return ""
    }
}

fun checkPermissionResult(grantResults: IntArray, success: () -> Unit, denied: (() -> Unit)? = null) {
    if (grantResults.isNotEmpty() && grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
        success.invoke()
    } else {
        denied?.invoke()
    }
}

fun Activity.checkRuntimePermission(permissions: List<String>, requestCode: Int, granted: () -> Unit, denied: (() -> Unit)? = null) {
    fun Activity.invokeSystemDialog(permissions: List<String>, requestCode: Int) {
        ActivityCompat.requestPermissions(
                this,
                permissions.toTypedArray(),
                requestCode
        )
    }

    if (permissions.all { ContextCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED }) {
        if (permissions.all { ActivityCompat.shouldShowRequestPermissionRationale(this, it) }) {
            denied?.invoke() ?: invokeSystemDialog(permissions, requestCode)
        } else {
            invokeSystemDialog(permissions, requestCode)
        }
    } else {
        granted.invoke()
    }
}

fun Fragment.checkRuntimePermission(permissions: List<String>, requestCode: Int, granted: () -> Unit, denied: (() -> Unit)? = null) {
    fun invokeSystemDialog(permissions: List<String>, requestCode: Int) {
        requestPermissions(
                permissions.toTypedArray(),
                requestCode
        )
    }

    context?.let {
        if (permissions.all { ContextCompat.checkSelfPermission(context!!, it) != PackageManager.PERMISSION_GRANTED }) {
            if (permissions.all { shouldShowRequestPermissionRationale(it) }) {
                denied?.invoke() ?: invokeSystemDialog(permissions, requestCode)
            } else {
                invokeSystemDialog(permissions, requestCode)
            }
        } else {
            granted.invoke()
        }
    }
}