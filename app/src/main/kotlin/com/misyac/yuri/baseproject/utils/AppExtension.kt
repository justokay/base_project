package com.misyac.yuri.baseproject.utils

import android.util.Base64
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.misyac.yuri.baseproject.di.common.AppExecutors
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers


fun ImageView.loadAvatar(url: String?) {
    Glide.with(context)
            .load(url)
            .into(this)
}

fun ImageView.loadImage(url: String?) {
    Glide.with(context)
            .load(url)
            .into(this)
}

fun createBase64Auth(email: String, password: String): String {
    val credentials = email + ":" + password
    return "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
}

fun getInitials(firstName: String?, lastName: String?): String {
    return StringBuilder().apply {
        firstName?.takeIf { length > 0 }?.let {
            append(it[0])
        }
        lastName?.takeIf { length > 0 }?.let {
            append(it[0])
        }
    }.toString()
}

fun <T> Single<T>.addDiskSchedulers(executor: AppExecutors): Single<T> {
    return this
            .subscribeOn(Schedulers.from(executor.diskIO()))
            .observeOn(Schedulers.from(executor.mainThread()))
}

fun <T> Single<T>.addNetworkSchedulers(executor: AppExecutors): Single<T> {
    return this
            .subscribeOn(Schedulers.from(executor.networkIO()))
            .observeOn(Schedulers.from(executor.mainThread()))
}

fun <T> Flowable<T>.addDiskSchedulers(executor: AppExecutors): Flowable<T> {
    return this
            .subscribeOn(Schedulers.from(executor.diskIO()))
            .observeOn(Schedulers.from(executor.mainThread()))
}
