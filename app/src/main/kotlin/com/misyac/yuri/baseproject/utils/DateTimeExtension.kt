package com.misyac.yuri.baseproject.utils

import java.util.*

fun Date.getWithStartOfDayTime() : Date {
    val calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"), Locale.getDefault())
    calendar.time = this
    calendar.set(Calendar.HOUR_OF_DAY, 0)
    calendar.set(Calendar.MINUTE, 0)
    calendar.set(Calendar.SECOND, 0)
    calendar.set(Calendar.MILLISECOND, 0)

    return calendar.time
}

fun Date.getWithEndOfDayTime() : Date {
    val calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"), Locale.getDefault())
    calendar.time = this
    calendar.set(Calendar.HOUR_OF_DAY, 23)
    calendar.set(Calendar.MINUTE, 59)
    calendar.set(Calendar.SECOND, 59)

    return calendar.time
}
