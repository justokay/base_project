package com.misyac.yuri.baseproject.utils

import java.text.NumberFormat

/**
 * Extensions to simplify formatting digits
 */

fun Int.formatForView() = NumberFormat.getIntegerInstance().format(this)

fun Int.toStringWithOrdinal() = this.toString() + this.ordinalSuffix()

/**
 * Locale specific function, consider this while localizing the app other than English
 */
fun Int.ordinalSuffix(): String {
    val hundredRemainder = this % 100
    val tenRemainder = this % 10
    if (hundredRemainder - tenRemainder == 10) {
        return "th"
    }

    return when (tenRemainder) {
        1 -> "st"
        2 -> "nd"
        3 -> "rd"
        else -> "th"
    }
}
