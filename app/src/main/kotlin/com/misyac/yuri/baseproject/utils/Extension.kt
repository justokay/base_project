package com.misyac.yuri.baseproject.utils

import android.os.Build
import android.text.Html
import android.text.Spanned
import java.util.*
import java.util.concurrent.TimeUnit

fun <T1 : Any, T2 : Any, R : Any> let2(p1: T1?, p2: T2?, block: (T1, T2) -> R?): R? {
    return if (p1 != null && p2 != null) block(p1, p2) else null
}

fun Int.toHex(): String {
    return String.format("#%06X", 0xFFFFFF and this)
}

private val MILLIS_PER_DAY = (24 * 60 * 60 * 1000).toLong()

fun Date.getDaysBetweenDates(date2: Date): Int {
    val difference = (this.time / MILLIS_PER_DAY - (date2.time / MILLIS_PER_DAY).toInt()).toInt()
    return Math.abs(difference)
}

fun Date?.utilDateToSqlLong(): Long {
    return if (this == null) {
        -1
    } else TimeUnit.MILLISECONDS.toSeconds(this.time)
}

fun Long.sqlLongToUtilDate(): Date? {
    return if (this < 0) {
        null
    } else Date(TimeUnit.SECONDS.toMillis(this))
}

fun Date.convertDate(vararg fields: Int): Date {
    val calendar = Calendar.getInstance()
    if (fields != null) {
        calendar.clear()
        val tempCalendar = Calendar.getInstance()
        tempCalendar.time = this
        for (field in fields) {
            calendar.set(field, tempCalendar.get(field))
        }
    } else {
        calendar.time = this
    }
    return calendar.time
}

fun Date.onlyDate(): Date {
    return convertDate(Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH)
}

fun CharSequence?.trimTrailingWhitespace(): CharSequence {
    if (this == null)
        return ""

    var i = this.length

    // loop back to the first non-whitespace character
    while (true) {
        if (!(--i >= 0 && Character.isWhitespace(this[i]))) break
    }

    return this.subSequence(0, i + 1)
}

fun String.fromHtml(): Spanned {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
    } else {
        Html.fromHtml(this)
    }
}

fun String.fromHtmlToString(): String {
    return fromHtml().toString().trim({ it <= ' ' })
}

fun Spanned.toHtml(): String {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        return Html.toHtml(this, Html.TO_HTML_PARAGRAPH_LINES_CONSECUTIVE);
    } else {
        Html.toHtml(this)
    }
}