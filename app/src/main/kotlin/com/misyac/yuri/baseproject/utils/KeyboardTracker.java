package com.misyac.yuri.baseproject.utils;


import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewTreeObserver;

import timber.log.Timber;

public class KeyboardTracker {

    private Activity mActivity;
    private OnKeyboardHeightChangeListener mOnKeyboardChangeListener;

    private boolean mListenerAttached = false;
    private View mRootView;

    private ViewTreeObserver.OnGlobalLayoutListener mKeyboardLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            boolean isLogging = false;

            // navigation bar height
            int navigationBarHeight = 0;
            int resourceId = getResources().getIdentifier("config_showNavigationBar", "bool", "android");
            if (isLogging) {
                if (resourceId > 0) {
                    boolean showNavBar = getResources().getBoolean(resourceId);
                    Timber.d("showNavBar = " + showNavBar);
                }
                boolean hasSoftKey = ViewConfiguration.get(mActivity).hasPermanentMenuKey();
                Timber.d("hasSoftKey = " + hasSoftKey);
            }

            resourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android");
            if (resourceId > 0) {
                navigationBarHeight = getResources().getDimensionPixelSize(resourceId);
            }

            // status bar height
            int statusBarHeight = 0;
            resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                statusBarHeight = getResources().getDimensionPixelSize(resourceId);
            }

            // display window size for the app layout
            Rect rect = new Rect();
            mActivity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);

            if (isLogging) {
                Timber.d("navigation bar height = " + navigationBarHeight);
                Timber.d("status bar height = " + statusBarHeight);
                Timber.d("window visible display frame height = " + rect.height());
                Timber.d("root view height = " + mRootView.getRootView().getHeight());
            }
            // screen height - (user app height + status + nav) ..... if non-zero, then there is a soft keyboard
            int keyboardHeight = mRootView.getRootView().getHeight() - (statusBarHeight + navigationBarHeight + rect.height());

//            if (Math.abs(keyboardHeight) == Math.abs(navigationBarHeight)) {
//                keyboardHeight = 0;
//            }

            mOnKeyboardChangeListener.onKeyboardHeightChange(keyboardHeight);
        }
    };

    public KeyboardTracker(Activity activity, OnKeyboardHeightChangeListener onKeyboardChangeListener) {
        mActivity = activity;
        mOnKeyboardChangeListener = onKeyboardChangeListener;

        start();
    }

    private Resources getResources() {
        return mActivity.getResources();
    }

    public void start() {
        if (mListenerAttached) {
            return;
        }

//        mRootView = mActivity.findViewById(R.id.root);
        mRootView.getViewTreeObserver().addOnGlobalLayoutListener(mKeyboardLayoutListener);

        mListenerAttached = true;
    }

    @SuppressWarnings("deprecation")
    public void stop() {
        if (mListenerAttached) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                mRootView.getViewTreeObserver().removeOnGlobalLayoutListener(mKeyboardLayoutListener);
            } else {
                mRootView.getViewTreeObserver().removeGlobalOnLayoutListener(mKeyboardLayoutListener);
            }
            mListenerAttached = false;
        }
    }

    public interface OnKeyboardHeightChangeListener {
        void onKeyboardHeightChange(int height);
    }
}
