package com.misyac.yuri.baseproject.utils

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import android.support.transition.TransitionManager
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.URLSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.view.inputmethod.InputMethodManager.SHOW_IMPLICIT
import android.widget.EditText
import android.widget.TextView


fun ViewGroup.inflateView(id: Int, attachToRoot: Boolean = false): View = LayoutInflater.from(this.context).inflate(id, this, attachToRoot)

fun View.show() {
    visibility = View.VISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun View.hide() {
    visibility = View.INVISIBLE
}

fun View.show(isShow: Boolean) {
    if (isShow) show() else gone()
}

fun View.setVisibleIfNeeded(): Boolean {
    if (!isVisible()) {
        show()
        return true
    }
    return false
}

fun View.hideIfNeeded() {
    if (visibility != View.INVISIBLE) {
        visibility = View.INVISIBLE
    }
}

fun View.showIfNeeded() {
    if (visibility != View.VISIBLE) {
        visibility = View.VISIBLE
    }
}

fun View.goneIfNeeded() {
    if (visibility != View.GONE) {
        visibility = View.GONE
    }
}

fun View.setGoneIfNeeded() {
    if (isVisible()) {
        gone()
    }
}

fun View.isVisible(): Boolean = visibility == View.VISIBLE

fun View.isGone(): Boolean = visibility == View.GONE

fun View.setVisibleOrGone(isVisible: Boolean) {
    if (isVisible) {
        setVisibleIfNeeded()
    } else {
        setGoneIfNeeded()
    }
}

/**
 * For root view
 */
fun View?.setSystemUiLightStatusBar() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        this?.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }
}

fun TextView.makeExpandableTextView(container: ViewGroup,
                                    maxLine: Int, listener: ReadMoreClickListener) {
    tag = text

    val clickableText = "... read more"

    if (maxLine in 1..(lineCount - 1)) {
        val lineEndIndex = layout.getLineEnd(maxLine - 1)
        val text = text.subSequence(0, lineEndIndex - clickableText.length + 1).toString() + clickableText

        val content = SpannableString(text)
        content.setSpan(ReadMoreSpan(context, container, listener),
                text.length - clickableText.length + 4, text.length, 0)

        val movementMethod = movementMethod
        if (movementMethod == null || movementMethod !== LinkMovementMethod.getInstance()) {
            setMovementMethod(LinkMovementMethod.getInstance())
        }
        setText(content)
    }

}

fun EditText.requestTouchWithKeyboard() {
    requestFocusFromTouch()
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(this, SHOW_IMPLICIT)
}

fun EditText.showSoftKeyboard() {
    val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

fun EditText.setSelectionToEnd() {
    setSelection(this.text.length)
}

fun EditText.hideSoftKeyboard() {
    val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(this.windowToken, 0)
}

fun EditText.setOnDoneAction(function: () -> Unit) {
    setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            function.invoke()
            hideSoftKeyboard()
            return@OnEditorActionListener true
        }
        false
    })
}

fun EditText.getTrimText(): String = text.toString().trim()

private class ReadMoreSpan(
        private val сontext: Context,
        private val сontainer: ViewGroup,
        private val listener: ReadMoreClickListener?) : ClickableSpan() {

    override fun updateDrawState(ds: TextPaint) {
        ds.isUnderlineText = false
//        ds.typeface = ResourcesCompat.getFont(сontext, R.font.gotham_rounded_book)
    }

    override fun onClick(widget: View) {
        TransitionManager.beginDelayedTransition(сontainer)
        widget.post {
            (widget as TextView).setText(widget.getTag().toString(),
                    TextView.BufferType.SPANNABLE)
        }
        listener?.onClickReadMore()
    }
}

interface ReadMoreClickListener {
    fun onClickReadMore()
}

fun TextView.setTextViewDrawableColor(color: Int) {
    compoundDrawables
            .filterNotNull()
            .forEach { it.colorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN) }
}

fun TextView.showIsNotNullOrEmpty(text: String?) {
    text?.takeIf { it.isNotEmpty() }?.let {
        this.text = it
        show()
    } ?: gone()
}

fun View.setWidthLayoutParams(width: Int) {
    this.layoutParams = this.layoutParams.apply {
        this.width = width
    }
}

fun View.setHeightLayoutParams(height: Int) {
    this.layoutParams = this.layoutParams.apply {
        this.height = height
    }
}

fun TextView.stripUnderlines() {
    val s = this.text as Spannable
    val spans = s.getSpans(0, s.length, URLSpan::class.java)
    for (span in spans) {
        var sp = span
        val start = s.getSpanStart(sp)
        val end = s.getSpanEnd(sp)
        s.removeSpan(sp)
        sp = URLSpanNoUnderline(sp.url)
        s.setSpan(sp, start, end, 0)
    }
    this.text = s
}

private class URLSpanNoUnderline : URLSpan {

    constructor(src: Parcel) : super(src) {}

    constructor(url: String) : super(url) {}

    override fun updateDrawState(ds: TextPaint) {
        super.updateDrawState(ds)
        ds.isUnderlineText = false
    }

    companion object {
        val CREATOR: Parcelable.Creator<URLSpanNoUnderline> = object : Parcelable.Creator<URLSpanNoUnderline> {
            override fun createFromParcel(`in`: Parcel): URLSpanNoUnderline {
                return URLSpanNoUnderline(`in`)
            }

            override fun newArray(size: Int): Array<URLSpanNoUnderline?> {
                return arrayOfNulls(size)
            }
        }
    }
}


